package pages;

import base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends TestBase {

    private static final String loginButtonClass = "btnSignIn";
    private static final String testPageTitle = "n11.com - Alışverişin Uğurlu Adresi";


    public HomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }


    public void goToHomePage() {

        driver.get(prop().getProperty("url"));
    }

    public boolean checkOfOnPage() {
        return driver.getTitle().contentEquals(testPageTitle);
    }

    public void goToLoginPage() {
        driver.get(prop().getProperty("url"));
        driver.findElement(By.className(loginButtonClass)).click();
    }
}
